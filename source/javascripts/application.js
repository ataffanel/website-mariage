'use strict';

// Logic for the howtocome page
function selectable(collection, callback) {
  collection.on('click', function() {
    $(this).addClass("selected");
    collection.not($(this)).removeClass("selected");

    callback();
  });
}

function updateHowtocome() {
  var region = $(".region .selected")[0]
  var transport = $(".transport .selected")[0]

  if (!region || !transport) return;

  var interest = $(".instruction." + region.id + "-" + transport.id).concat($(".instruction."+region.id),
                                                                        $(".instruction."+transport.id))

  $(interest).addClass("shown");
  $(".instruction").not($(interest)).removeClass("shown");
  // Scroll the page to the instructions
  $("#instructions")[0].scrollIntoView();

  // Update the location in the URL
  window.location.hash = region.id + "-" + transport.id;
}

// initialize the choice state out of the hash from the URL
function initializeHowtocome() {
  var interest = window.location.hash.replace('#', '').split('-')

  if (interest.length == 2) {
    // Some sanity check!
    var regions = $(".region div").map(function() { return $(this).attr('id'); })
    var transports = $(".transport div").map(function() { return $(this).attr('id'); })
    if ($.inArray(interest[0], regions) < 0 || $.inArray(interest[1], transports) < 0) {
      return;
    }

    $(".region #" + interest[0]).addClass("selected");
    $(".transport #" + interest[1]).addClass("selected");

    updateHowtocome();
  }
}

function updateStory() {
  $(".storyItem").not($(window.location.hash)).addClass("hidden");
  $(window.location.hash).removeClass("hidden");
  $(window.location.hash)[0].scrollIntoView();
  var linkId = "#link" + window.location.hash.replace('#', '');
  $('.storylink').not($(linkId)).removeClass("active");
  $(linkId).addClass("active");
}

function initializeStoryPage() {
  if ($(".story").length == 0) return;

  if (window.location.hash.length == 0) {
    window.location.hash = $(".storyItem")[0].id;
  }

  window.onhashchange = function(event) {
    event.preventDefault();
    updateStory();
  };

  updateStory();
}

function updatePhotos() {
  var current = window.location.hash;
  if (current == "") {
    current = "#" + $(".photo-group")[0].id;
  }
  $(".photo-group").not($(current)).addClass("hidden");
  $(current).removeClass("hidden");
}

function initializePhotosPage() {
  if ($(".photos").length == 0) return;

  window.onhashchange = function(event) {
    event.preventDefault();
    updatePhotos();
  };

  // if (window.location.hash.length == 0) {
  //   window.location.hash = $(".photo-group")[0].id;
  // }

  updatePhotos();
}

// On page ready
$(function() {
  initializeHowtocome();
  selectable($(".region div"), updateHowtocome);
  selectable($(".transport div"), updateHowtocome);

  // Enable lity image pop-up on all images with class view
  $(".view").on("click", function() {
     lity(this.src);
  });

  initializeStoryPage();
  initializePhotosPage();
})
