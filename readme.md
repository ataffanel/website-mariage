Mariage website
===============

Setting up (to do only the first time)
----------
To do only the very first time.

From within the project folder:
```
sudo apt-get install ruby bundler nodejs
bundler install
```

Working with the website
------------------------
To be done everytime before to change anything.

```
cd         #Open the project folder in the terminal.
git pull   # get latest version
bundler exec middleman serve
```

Now browse to http://127.0.0.1:4567/ to see the website.

Ctrl-c to stop.

Working with the repos
----------------------

Modified files needs to be 'added'. Then the added file can be commited with a
commit message and finally it can be pushed to the remote server:

```
git status                               # see what is changed
git add xxxxxxxx.erb            # add file(s) to be commited
git commit -m 'Add ... to the frontpage' # Commit with a message
git push                                 # push the commit to the remote server
```

When the commit is pushed, the website will be updated within minutes.

Renewing the certificate
------------------------

Every 3 month the certificate needs to be renewed. The Certbot command for it is:
```
certbot certonly --manual --config-dir config/ --logs-dir logs/ --work-dir work/ --agree-tos -d arnaudetemilie.fr -d www.arnaudetemilie.fr --email arnaud@taffanel.org
```
