# require 'compass/import-once/activate'
# Require any additional compass plugins here.
# require 'breakpoint'

# Set this to the root of your project when deployed:
http_path = "/"
css_dir = "stylesheets"
sass_dir = "sass"
images_dir = "images"
javascripts_dir = "javascripts"

# You can select your preferred output style here (can be overridden via the command line):
# output_style = :expanded or :nested or :compact or :compressed

# To enable relative paths to assets via compass helper functions. Uncomment:
# relative_assets = true

# To disable debugging comments that display the original location of your selectors. Uncomment:
# line_comments = false


# If you prefer the indented syntax, you might want to regenerate this
# project again passing --syntax sass, or you can uncomment this:
# preferred_syntax = :sass
# and then run:
# sass-convert -R --from scss --to sass sass scss && rm -rf sass && mv scss sass

sourcemap = true

## Middleman configuration

# Per-page layout changes:
#
# With no layout
page '/*.xml', layout: false
page '/*.json', layout: false
page '/*.txt', layout: false

# General configuration

activate :autoprefixer do |config|
  config.browsers = ['> 5%', 'Explorer >= 9']
end

# Reload the browser automatically whenever files change
configure :development do
  activate :livereload
end

activate :i18n, :mount_at_root => :fr

set :relative_links, true

# Build-specific configuration
configure :build do
  set :build_dir, 'public'
  set :base_url, "" # Site accessible on the root of www.arnaudetemilie.fr
  activate :relative_assets # Use relative URLs
  # For example, change the Compass output style for deployment
  activate :minify_css
  # Minify Javascript on build
  activate :minify_javascript
  # Enable cache buster
  # activate :asset_hash
  # Use relative URLs
  # activate :relative_assets
  # Or use a different image path
  # set :http_prefix, "/Content/images/"
  activate :minify_html
end

require 'git'

helpers do
  #
  # returns the correct path in the current locale
  # @example
  # = link_to "bar", url("foo/bar.html")
  #
  def path(url, options = {})
    lang = options[:lang] || I18n.locale.to_s

    if lang.to_s == 'fr'
      prefix = ''
    else
      prefix = "/#{lang}"
    end

    base = config[:base_url] || ''

    base + prefix + "/" + clean_from_i18n(url)
  end

  # removes an i18n lang code from url if its present
  def clean_from_i18n(url)
    parts = url.split('/').select { |p| p && p.size > 0 }
    parts.shift if langs.map(&:to_s).include?(parts[0])

    parts.join('/')
  end

  # get the current git hash version

  def git_version()
    g = Git.open('.')
    g.gcommit('HEAD').sha
  end
end
